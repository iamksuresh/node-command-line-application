const program = require("commander");
const { 
    validateUserInput,
    SpaceRegex,
    generateNumbersArray,
    upperLeftCoordinates,
} = require('./utils');
const FillColor = 'x';

const LineInputquestions = [
    {
        type: "input",
        name: "start",
        message: `Enter space separated numerical coordinates(x1 y1) ex-(10 5) : \n`,
        validate: function(value) {
            /* Check for space */
            let coordinates = upperLeftCoordinates(value, program);
            if(coordinates){
                /* save Line cordinates in instance obj */
                program.lineCordinates = {...coordinates}; 
                return true;
            } else {
                return `Enter space separated numerical value  example:(10 1).\n Please note : \n  1 <= X <= ${program.maxWidth} \n  1 <= Y <= ${program.maxHeight} \n`;
            }
        
        }
    },
    {
        type: "input",
        name: "end",
        message: `Enter space separated numerical coordinates(x2 y2) ex-(10 5) : \n`,
        validate: function(value) {
        if (validateUserInput(value, SpaceRegex)) {
            let splitVal = value.split(" ");
            let x2 = parseInt(splitVal[0]);
            let y2 = parseInt(splitVal[1]);            
            if (
            !(
                program.lineCordinates.x1 === x2 || program.lineCordinates.y1 === y2
            )
            ) {
            return `Either of X or Y co-ordinates should share same value to draw a line.`;
            } else if( (program.lineCordinates.x1 === x2 && program.lineCordinates.y1 === y2) ) {
            return 'Both coordinates cannot be the same'
            }else if (
            x2 !== 0 &&
            y2 !== 0 &&
            x2 <= program.maxWidth &&
            y2 <= program.maxHeight
            ) {
            program.lineCordinates = {
                ...program.lineCordinates,
                x2,
                y2
            };
            return true;
            }
        }
        return `Enter space separated numerical value  example:(10 1). \n  \n  1 <= X <= ${program.maxWidth} \n  1 <= Y <= ${program.maxHeight}\n`;
        }
    }
];

const drawLine = () => {
    try {
        let canvas = program.canvas;
        const { x1, y1, x2, y2 } = program.lineCordinates;
        let row = x1;
        let generateNosArr = [];
        let obj = { ...program.blockedIndex };
        if (x1 === x2) {
        /* vertical line */
        generateNosArr = generateNumbersArray(y1,y2)
        /* create canvas */
        row = x1;
        generateNosArr.forEach(v => {
            canvas[v] = canvas[v].split("");
            canvas[v][row] = FillColor;
            if (obj[v]) {
            obj[v] = Array.from(new Set([...obj[v], row])).sort();
            } else {
            obj[v] = [row];
            }
            canvas[v] = canvas[v].join("");
        });
        } else if (y1 === y2) {
        /* horizontal line */        
        generateNosArr = generateNumbersArray(x1,x2)
        row = y1;
        generateNosArr.forEach(v => {
            canvas[row] = canvas[row].split("");
            canvas[row][v] = FillColor;
            if (obj[row]) {
            obj[row] = Array.from(new Set([...obj[row], v])).sort();
            } else {
            obj[row] = [v];
            }
            canvas[row] = canvas[row].join("");
        });
        }
        program.canvas = canvas;
        program.blockedIndex = { ...obj };
    } catch (e) {
        console.error("error in drawing line :  ", e);
    }
  };

module.exports = {
    drawLine,
    LineInputquestions,
}