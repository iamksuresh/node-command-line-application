const program = require("commander");
const { 
    showOutput,
    validateUserInput,  
    SpaceRegex,
    generateNumbersArray,
    upperLeftCoordinates,
} = require('./utils');
const { drawLine } = require('./drawLine');
const RectangleInputquestions = [ 
    {
      type: "input",
      name: "start",
      message: `Enter space separated numerical coordinates(x1 y1) example-(10 5) : \n`,
      validate: function(value) {
        /* Check for space */
        let coordinates = upperLeftCoordinates(value, program);
        if(coordinates){
            /* save Line cordinates in instance obj */
            program.rectangleCordinates = {...coordinates}; 
            return true;
        } else {
          return `Enter space separated numerical value example:(10 1). \n Please note : \n  1 <= X <= ${program.maxWidth} \n  1 <= Y <= ${program.maxHeight} \n`;
        }
      }
    },
    {
      type: "input",
      name: "end",
      message: `Enter space separated numerical coordinates(x2 y2) example-(10 5) : \n`,
      validate: function(value) {
        let splitVal = value.split(" ");
        let x2 = parseInt(splitVal[0]);
        let y2 = parseInt(splitVal[1]);
        if (validateUserInput(value, SpaceRegex) && (x2 !== program.rectangleCordinates.x1 && y2 !== program.rectangleCordinates.y1)) {
          if (
            x2 !== 0 &&
            y2 !== 0 &&
            x2 <= program.maxWidth &&
            y2 <= program.maxHeight
          ) {
            program.rectangleCordinates = {
              ...program.rectangleCordinates,
              x2,
              y2
            };
            return true;
          }
        }
        return `Enter space separated numerical value example:(10 1).\n NOTE : \n Neither of X or Y co-ordinates should share same value to draw a rectangle. \n  1 <= X <= ${
          program.maxWidth
        } \n  1 <= Y <= ${program.maxHeight} \n`;
      }
    }
];

const drawRectangle = () => {
    try {
        const { x1, y1, x2, y2 } = program.rectangleCordinates;
        let obj = { ...program.blockedIndex };
        /* 
          construct vertices for rectangle.
          Logic : Take 2 coordinates (x1,y1) (x2,y2) and generate 4 vertices of the rectangle
            - (x1,y1) (x1,y2)
            - (x1,y1) (x2,y1)
            - (x2,y2) (x1,y2)
            - (x2,y2) (x2,y1)
        */
        let xPoints = [{ x1, y1 }, { x2, y2 }];
        let yPoints = [{ x1, y2 }, { x2, y1 }];
        xPoints.forEach((val, idx) => {
        let xKeys = Object.values(val);
        let x1 = xKeys[0];
        let y1 = xKeys[1];
        /* Draw four vertices of rectangle using Line function */
        yPoints.forEach((yVal, yIdx) => {
            let temp = Object.values(yVal);
            let x2 = temp[0];
            let y2 = temp[1];
            let obj = {x1,y1,x2,y2};
            program.lineCordinates = { ...obj };
            /* use line function to draw vertices of rectangle */
            drawLine();
        });
        });
        /* display rectangle in the console */
        showOutput(program.canvas);
        /*  
            Generate blocked indexes. 
            'blockedIndex' object will be used as reference to know the filled indices when filling the canvas.
        */
        let allRows = generateNumbersArray(y1,y2)
        let indexes = generateNumbersArray(x1,x2)
        allRows.forEach(v => {
            if (obj[v]) {
                obj[v] = Array.from(new Set([...obj[v], ...indexes])).sort();
            } else {
                obj[v] = indexes;
            }
        });
        program.blockedIndex = { ...obj };
    } catch (e) {
      console.log("error in drawing rectangle :  ", e);
    }
};

module.exports = {
    drawRectangle,
    RectangleInputquestions,
}