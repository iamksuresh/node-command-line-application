
# Tech challenge :
-   This is an interactive node.js command line application.
-   This interactive application prompts user for input to draw rectangle , Line (horizontal, vertical),canvas,fill the canvas, reset the canvas
-   To make application interactive, commands are replaced by interactive prompts.
-   Test cases are written using Jest
### start the application
- clone the application - git clone git@bitbucket.org:iamksuresh/node-command-line-application.git
- In the terminal , execute the following    
    - cd <root directory>
    - npm install
    - npm start  
-   Testing the application    
    - cd <root directory>
    - npm run test

### Limitations
-   For the purpose of demo , max width and max height of canvas is restricted to 100
-   Diagrams can overlap each other.
-   output is shown using console.log()
-   This repo is made public for the purpose of sharing the code with the reviewers.

### Tested IDE
-   command prompt
-   powershell
-   vs code - Terminal

For further clarifications, please reach out to Suresh (suresh.jain@ymail.com / sureshvinayakiya@gmail.com)
