const program = require("commander");
const { prompt } = require("inquirer");
const { 
    showOutput,
    validateUserInput,
    Range,
    drawHorizontal,
    drawVertical,
} = require('./utils');
const { askQuestions } = require('./listQuestions');

const canvasInputValidate = (value = '',lowerlimit = 0) => {
    if (validateUserInput(value) && Range(value,lowerlimit)) {
        return true;
    }
    return false;
}

const canvasInputquestions = [
    {
      type: "input",
      name: "width",
      message: "Enter width of the canvas ...",
      validate: function(value) {
        return canvasInputValidate(value)
            ? true
            : "For width, please enter a valid number 2 < N <= 100";
      }
    },
    {
      type: "input",
      name: "height",
      message: "Enter height of the canvas ...",
      validate: function(value) {
        return canvasInputValidate(value,1)
            ? true
            : "For height, please enter a valid number 1 < N <= 100";
      }
    }
];

const drawCanvas = () => {
    try {
      prompt(canvasInputquestions).then(answers => {
        let i = 0;
        let output = [];
        /* saving maxwidth and maxheight of canvas in Program instance obj. */
        program.maxWidth = parseInt(answers.width - 2);
        program.maxHeight = parseInt(answers.height);
        output.push(drawHorizontal(answers));
        do {
          output.push(drawVertical(answers));
          i++;
        } while (i < parseInt(answers.height));
        output.push(drawHorizontal(answers));
        /* Saving unmodified canvas in 'originalCanvas' for reset option */
        program.originalCanvas = [...output];
        program.canvas = [...output];
        showOutput(program.canvas);
        program.blockedIndex = {};
        program.canvasFilled = false; 
        askQuestions();
      });
    } catch (e) {
      console.log("error in canvas");
    }
};

module.exports = {
    drawCanvas,
    canvasInputquestions,
    canvasInputValidate,
}
