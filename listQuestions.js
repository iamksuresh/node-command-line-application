const program = require("commander");
const { prompt } = require("inquirer");
const {
    drawRectangle,
    RectangleInputquestions,
} = require('./drawRectangle');
const {
    drawLine,
    LineInputquestions,
} = require('./drawLine');
const { 
    isCanvasFilled,
    showOutput,
    fillCanvas,
} = require('./utils');

const listOptions = [
    {
      type: "list",
      name: "answer",
      message: "\n Awesome, our canvas is ready. What would you like to draw",
      choices: () => constructList()
    }
];

const fillColor = [
  {
      type: "input",
      name: "fill",
      message: `Enter a character to the canvas : \n`,
      validate: function(value) {
          /* Check for space */
          let regex = /^\S$/
          if(regex.test(value)){
              /* save color in instance obj */
              program.canvasFillColor = value; 
              return true;
          } else {
              return 'Only non-whitespace single character is accepted'
          }      
      }
  },
]
const constructList = () => {
    let options = [
      {
        name: "rectangle",
        disabled:
          program.maxWidth < 2 || program.maxHeight < 2 || program.canvasFilled || isCanvasFilled()
            ? "DISABLED : cavas area is too small to draw rectangle (or) canvas is filled."
            : false
      },
      {
        name: "line",
        disabled: program.canvasFilled || isCanvasFilled() ? "DISABLED : canvas is filled." : false
      },
      "exit the program",
      {
        name: "reset canvas",
        disabled:
        program.canvasFilled
            ? false 
            :  Object.keys(program.blockedIndex).length <= 0 
                ? "DISABLED : Please draw on the canvas to reset it."
                : false
      },
      {
        name: "fill the canvas",
        disabled: program.canvasFilled || isCanvasFilled() ? "DISABLED : canvas is filled." : false
      }
  
    ];
    return options;
};
  
const askQuestions = () => {
    try {
      prompt(listOptions).then(({ answer = "line" }) => {
        switch (answer) {
          case "rectangle": {
            prompt(RectangleInputquestions).then(ans => {
              drawRectangle();
              askQuestions()
            });  
            break;
          }
          case "line": {
            prompt(LineInputquestions).then(ans => {
              drawLine();
              showOutput(program.canvas);
              askQuestions();
            });
            break;
          }
          case "reset canvas": {
            program.canvas = [...program.originalCanvas];
            program.blockedIndex = {};
            program.canvasFilled = false;
            console.clear()
            showOutput(program.canvas);
            askQuestions();
            break;
          }
          case "fill the canvas": {
            prompt(fillColor).then(ans => {
              let fillerCanvas = fillCanvas(program);
              console.log()
              showOutput(fillerCanvas);
              askQuestions();
            })            
            break;
          }
          case "exit the program": {
            console.clear()
            process.exit();
          }
        }
      });
    } catch (e) {
      console.log("error in input : ", e);
    }
};

module.exports = {
    askQuestions,
}