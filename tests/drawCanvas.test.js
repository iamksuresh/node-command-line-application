const { canvasInputValidate, canvasInputquestions } = require('../drawCanvas');
const inquirer = require("inquirer");

jest.mock('inquirer');

describe('validate canvas input ', () => {
    test('case 1: no input ', () => {
        expect(canvasInputValidate()).toBeFalsy()
    })
    test('case 2: input 4 ', () => {
        expect(canvasInputValidate(4)).toBeTruthy()
    })
    test('case 2: lowerlimit > upperlimit ', () => {
        expect(canvasInputValidate(4, 102)).toBeFalsy()
    })
    test('case 3: lowerlimit < upperlimit ', () => {
        expect(canvasInputValidate(4, 2, 101)).toBeTruthy()
    })
})

describe('Test canvas input questions', () => {
    test('user input: case 1 -> width > 3 and height > 1', () => {
        let canvasInputquestions = { width: 3, height: 10 }
        inquirer.prompt = jest.fn( ({ width, height } = value = {}) => {
            if(width > 2) { 
                return {width}
            } else {
                return 'For width, please enter a valid number 2 < N <= 100';
            }            
        })        
        expect(inquirer.prompt(canvasInputquestions)).toEqual({ width: 3 });
    });
    test('user input: case 2 -> width is empty', () => {
        let canvasInputquestions = { width: '', height: 10 }
        inquirer.prompt = jest.fn( ({ width, height } = value = {}) => {
            if(width > 2) { 
                return {width}
            } else {
                return 'For width, please enter a valid number 2 < N <= 100';
            }            
        })        
        expect(inquirer.prompt(canvasInputquestions)).toEqual('For width, please enter a valid number 2 < N <= 100');
    });
    test('user input: case 3 -> width < 3 ', () => {
        let canvasInputquestions = { width: '', height: 10 }
        inquirer.prompt = jest.fn( ({ width, height } = value = {}) => {           
            if(width > 2) { 
                return {width}
            } else {
                return 'For width, please enter a valid number 2 < N <= 100';
            }            
        })        
        expect(inquirer.prompt(canvasInputquestions)).toEqual('For width, please enter a valid number 2 < N <= 100');
    });
    test('user input: case 4 -> width > 3 and height === 1', () => {
        //let canvasInputquestions = { width: '', height: 1 }
        inquirer.prompt = jest.fn( ({ width, height } = value = {}) => {           
            if(width > 2 && height > 1) { 
                return {width,height}
            } else {
                return "For height, please enter a valid number 1 < N <= 100";
            }            
        })        
        let x = inquirer.prompt(canvasInputquestions)
        expect(x).toEqual("For height, please enter a valid number 1 < N <= 100");
    });
});