const { LineInputquestions } = require('../drawLine');
const { 
    upperLeftCoordinates,
} = require('../utils');
const inquirer = require("inquirer");

describe('Test line input questions', () => {
    test('user input: case 1 -> empty input', () => {
        const program = {}
        inquirer.prompt = jest.fn( (value = '') => {   
            let coordinates = upperLeftCoordinates(value, program);      
            if(coordinates){
                return true;
            } else {
              return `Enter space separated numerical value  example:(10 1).\n Please note : \n  1 <= X <= ${program.maxWidth} \n  1 <= Y <= ${program.maxHeight} \n`;
            }     
                       
        })
        expect(inquirer.prompt(LineInputquestions)).toEqual(`Enter space separated numerical value  example:(10 1).\n Please note : \n  1 <= X <= ${program.maxWidth} \n  1 <= Y <= ${program.maxHeight} \n`);
    });

    test('user input: case 2 -> input (3 4)', () => {
        const program = {maxWidth:5,maxHeight:5};
        inquirer.prompt = jest.fn( (value = '') => {   
            let coordinates = upperLeftCoordinates(value, program);      
            if(coordinates){
                return true;
            } else {
              return `Enter space separated numerical value  example:(10 1).\n Please note : \n  1 <= X <= ${program.maxWidth} \n  1 <= Y <= ${program.maxHeight} \n`;
            }     
                       
        })
        expect(inquirer.prompt('3 4')).toBeTruthy();
    });

    test('user input: case 2 -> input (3 4) , no program obj', () => {
        const program = {};
        inquirer.prompt = jest.fn( (value = '') => {   
            let coordinates = upperLeftCoordinates(value, program);      
            if(coordinates){
                return true;
            } else {
              return `Enter space separated numerical value  example:(10 1).\n Please note : \n  1 <= X <= ${program.maxWidth} \n  1 <= Y <= ${program.maxHeight} \n`;
            }     
                       
        })
        expect(inquirer.prompt('3 4')).toEqual(`Enter space separated numerical value  example:(10 1).\n Please note : \n  1 <= X <= ${program.maxWidth} \n  1 <= Y <= ${program.maxHeight} \n`);
    });

})