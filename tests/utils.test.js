const { 
    validateUserInput,
    Range,
    drawHorizontal,
    drawVertical,
    isCanvasFilled,
    OnlyNumberRegex,
    SpaceRegex,
    fillCanvas,
    generateNumbersArray,
    upperLeftCoordinates,
} = require('../utils');

describe('Testing validateUserInput function ', () => {
    test('For numbers should return true' , () => {
        expect(validateUserInput(123)).toBe(true)
    })
    test('for string input should return false' , () => {
        expect(validateUserInput('1 23')).toBe(false)
    })
    test('For empty value should return false' , () => {
        expect(validateUserInput()).toBe(false)
    })
})

describe('Testing Range function ', () => {
    test('For empty input should return false' , () => {
        expect(Range()).toBe(false)
    })
    test('For 2 should return false' , () => {
        expect(Range(2)).toBe(false)
    })
    test('For 101 should return false' , () => {
        expect(Range(101)).toBe(false)
    })
    test('For 3 should return true' , () => {
        expect(Range(3)).toBe(true)
    })
    test('For 100 should return true' , () => {
        expect(Range(100)).toBe(true)
    })
})

describe('Testing drawHorizontal function ', () => {
    test('For empty input should return empty string' , () => {
        expect(drawHorizontal()).toBe("")
    }) 
    test('For 0 should return empty string' , () => {
        expect(drawHorizontal(0)).toBe("")
    })
    test('For 1 should return string "-"' , () => {
        expect(drawHorizontal({width:1})).toBe("-")
    }) 
    test('For 2 should return string "--"' , () => {
        expect(drawHorizontal({width:2})).toBe("--")
    }) 
})

describe('Testing drawVertical function ', () => {
    test('For empty input should return empty string' , () => {
        expect(drawVertical()).toBe("")
    }) 
    test('For 0 should return empty string' , () => {
        expect(drawVertical(0)).toBe("")
    })
    test('For 1 should return string "|"' , () => {
        expect(drawVertical({width:1})).toBe("|")
    }) 
    test('For 2 should return string "||"' , () => {
        expect(drawVertical({width:2})).toBe("||")
    }) 
    test('For 5 should return - |<3 spaces>|' , () => {
        expect(drawVertical({width:5})).toBe("|   |")
    }) 
})

describe('Testing isCanvasFilled function ', () => {
    test('For empty input should return empty string' , () => {
        expect(isCanvasFilled()).toBe(true)
    }) 
})

describe('Testing OnlyNumberRegex Regex ', () => {
    test('For empty input should return false' , () => {        
        expect('').not.toMatch(OnlyNumberRegex)
    }) 
    test('For string input should return false' , () => {
        expect('text').not.toMatch(OnlyNumberRegex)
    }) 
    test('For Number input should return true' , () => {
        expect('12').toMatch(OnlyNumberRegex)
    }) 
    test('For Alphanumeric input should return false' , () => {
        expect('12fd').not.toMatch(OnlyNumberRegex)
    }) 
    test('For stringwithspace input should return false' , () => {
        expect('12 fd').not.toMatch(OnlyNumberRegex)
    }) 
})

describe('Testing SpaceRegex Regex ', () => {
    test('For empty input should return false' , () => {
        expect('').not.toMatch(SpaceRegex)
    }) 
    test('For string input should return false' , () => {
        expect('text').not.toMatch(SpaceRegex)
    }) 
    test('For Number input should return false' , () => {
        expect('12').not.toMatch(SpaceRegex)
    }) 
    test('For Alphanumeric input should return false' , () => {        
        expect('12f').not.toMatch(SpaceRegex)
    }) 
    test('For MultiplespaceBetweenNumber input should return false' , () => {
        expect('12 3').toMatch(SpaceRegex)
    }) 
    test('For MultiplespaceBetweenNumber input should return false' , () => {
        expect('12  3').not.toMatch(SpaceRegex)
    }) 
})

describe('Testing fillCanvas Regex ', () => {
    test('fillCanvas - case 1' , () => {
        let arr = ['----','|  |','|  |','----']
        let pgmObj = {
            canvas : arr,
            blockedIndex: {},
        }
        let resultArr = ['----','||','||','----']
        expect(fillCanvas(pgmObj)).toEqual(expect.arrayContaining(resultArr))
    })
    test('fillCanvas - case 2' , () => {
        let pgmObj = {
            //canvas : arr,
            blockedIndex: {},
        }
        let resultArr = ['----','|  |','|  |','----']
        expect(fillCanvas(pgmObj)).not.toEqual(expect.arrayContaining(resultArr))
    })
})

describe('Testing generateNumbersArray function ', () => {
    test('generateNumbersArray - empty input should' , () => {
        expect(generateNumbersArray()).toEqual([0])
    })
    test('generateNumbersArray - case 1' , () => {
        expect(generateNumbersArray(5,9)).toEqual([5,6,7,8,9])
    })
    test('generateNumbersArray - case 2' , () => {
        expect(generateNumbersArray(12,8)).toEqual([8,9,10,11,12])
    })
})


describe('Testing upperLeftCoordinates function ', () => {
    test('upperLeftCoordinates - case 1 value = (4 4), no program obj' , () => {
        expect(upperLeftCoordinates('4 4')).toEqual(false)
    }) 

    test('upperLeftCoordinates - case 2 value = (4 4)' , () => {
        expect(upperLeftCoordinates('4 4',{maxWidth : 6, maxHeight: 10})).toEqual({x1: 4, y1: 4})
    })    
})