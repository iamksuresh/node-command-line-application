const program = require('commander');
const { drawCanvas } = require('./drawCanvas');

const createCanvas = () => {
    console.log("\nHi there !! welcome :)");
    console.log("Before we start, lets create a canvas \n");
    drawCanvas();
};


program
  .version("0.0.1")
  .description("Tech Task - Node js command line")
  .option("C", "create canvas", createCanvas);
  
program.parse(process.argv);