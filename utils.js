const program = require("commander");

const OnlyNumberRegex = /^\d+$/;
const SpaceRegex = /^[0-9]+\s[0-9]+$/;
const showOutput = output => {
    console.log()
    output.map(v => console.log(v));
};

const validateUserInput = (value, regex = OnlyNumberRegex) => {
    /* Allow only numbers */
    var pass = regex.test(value);
    return pass ? true : false;
};

/* For the purpose of challenge, fixing the range to 100 */
const Range = (val = 0, lowerlimit = 2, upperlimit = 100) =>
    lowerlimit < val && val <= upperlimit;

const drawHorizontal = ( {width = 0} =  answers = {}) => {
    return Array.from(Array(parseInt(width)).keys())
        .map(() => "-")
        .join("");
};
    
const drawVertical = ( {width = 0} =  answers = {}) => {
    return Array.from(Array(parseInt(width)).keys())
        .map(
        (v, i) =>
            (i === 0 && "|") || (i === parseInt(width) - 1 && "|") || " "
        )
        .join("");
};

const isCanvasFilled = () => {
    let totalFilledIdx = 0;
    const { maxWidth = 0, maxHeight = 0, blockedIndex = {}} = program
    let totalavailableIndexes = maxWidth * maxHeight;    
    let valuesArr = Object.values(blockedIndex);
    valuesArr.reduce( function(acc,curr){
        totalFilledIdx += curr.length;
    },valuesArr[0])
    return (totalavailableIndexes <= totalFilledIdx)
        ? true
        : false
}

const fillCanvas = ({canvas = [], blockedIndex = {}} = program = {}) => {
    try {
      let tempCanvas = [...canvas];
      canvas.forEach((curr, i) => {
        if (i === 0 || i === canvas.length - 1) {
          return;
        }
        let row = curr.split(""); // tempCanvas[i].split('') //curr.split('');
        if (blockedIndex[i]) {
          row.forEach((val, idx) => {
            if (idx === 0 || idx === row.length - 1) return;
            if (!blockedIndex[i].includes(idx)) {
              row[idx] = program.canvasFillColor;
            }
          });
        } else {
          row.forEach((val, idx) => {
            if (idx === 0 || idx === row.length - 1) return;
            row[idx] = program.canvasFillColor;
          });
        }
        tempCanvas[i] = row.join("");
      });
      program.canvas = tempCanvas;
      program.canvasFilled = true;
      return tempCanvas;
    } catch (e) {
      console.log("error in fill ", e);
    }
};

const generateNumbersArray = (y1 = 0, y2 = 0) => {
    let endIdx = Math.max(y1,y2);
    let startIdx = Math.min(y1,y2);
    return new Array(endIdx - startIdx + 1)
          .fill(1)
          .map((_, i) => i + startIdx);
}

const upperLeftCoordinates = (value,program={}) => {
  /* Return (x1,y1) coordinates */   
  const { maxWidth, maxHeight } = program
  if (validateUserInput(value, SpaceRegex)) {
      let splitVal = value.split(" ");
      let x1 = parseInt(splitVal[0]);
      let y1 = parseInt(splitVal[1]);
      if (
          x1 !== 0 &&
          y1 !== 0 &&
          x1 <= maxWidth &&
          y1 <= maxHeight
      ) {
          return  {
              x1,
              y1
          };;
      }
  } 
  return false;
}

module.exports = {
  showOutput,
  validateUserInput,
  Range,
  drawHorizontal,
  drawVertical,
  OnlyNumberRegex,
  SpaceRegex,
  isCanvasFilled,
  fillCanvas,
  generateNumbersArray,
  upperLeftCoordinates,
}